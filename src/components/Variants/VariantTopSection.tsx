import { urlFor } from '@/sanity'
import React from 'react'
import VariantMainImage from './VariantMainImage'
import VariantInfo from './VariantInfo'

interface Props{
    variant: any
  }

function VariantTopSection({variant}:Props) {
  return (
    <section
    className='flex w-full gap-10 p-14 justify-center'
    >
      <VariantMainImage
      imageUrl={variant?.mainImage ? urlFor(variant?.mainImage).url()! : `/Images/hado.jpg`}
      />
      <VariantInfo
      name={variant?.product}
      category={variant?.ProductType?.name}
      description={variant?.description}
      color={variant?.color?.name}
      size = {variant?.size?.value}

      />
    </section>
  )
}

export default VariantTopSection
