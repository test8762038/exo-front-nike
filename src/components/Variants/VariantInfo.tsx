import React from 'react'

function VariantInfo({name='Jordan 1', category='Chaussures', description='desc', color = 'red',size = 'EU 45', gallery = new Array()}:any) {
  return (
    <div
        className='flex flex-col py-3 space-y-1'
        >
            <h3
            className='font-medium '
            >
                {name}
            </h3>

            <p
            className='opacity-80 '
            >
                {category}
            </p>

            <p
            className='font-medium pt-1'
            >
                {color}
            </p>

            <p
            className='font-medium pt-1'
            >
                {size}
            </p>

            <p
            className='text-sm max-w-md'
            >
                {description}
            </p>
        </div>
  )
}

export default VariantInfo