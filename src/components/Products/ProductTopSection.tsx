import React from 'react'
import ProductMainImage from './ProductMainImage'
import ProductInfo from './ProductInfo'

interface Props{
  product: any
}

function ProductTopSection({product}:Props) {
  return (
    <section
    className='flex w-full gap-10 p-14 justify-center'
    >
      <ProductMainImage/>
      <ProductInfo
      product
      />
    </section>
  )
}

export default ProductTopSection
