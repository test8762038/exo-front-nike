import { ArrowLeftIcon, ArrowRightIcon, ChevronDownIcon, CurrencyDollarIcon, ShoppingCartIcon } from '@heroicons/react/24/outline'
import Image from 'next/image'
import React from 'react'
import HeaderJ from './HeaderJ'

function jordan() {
  return (
    <div
    className='w-screen h-screen flex flex-col bg-orange-500 pt'
    >
       <HeaderJ/>
       <div
        className='z-10 p-3 '
        >
            <ul
            className='flex justify-center space-x-10'
            >
                <li
                className=''
                >
                    <p
                    className='text-orange-300 text-xl font-semibold overflow-hidden'
                    >
                        {'Nike Air Max 2015'}
                    </p>
                </li>
                <li>
                    <div
                    className='flex space-x-4 items-center'
                    >
                        <ArrowLeftIcon
                        className='text-white w-6 h-6'
                        />
                        <div
                        className='flex flex-col'
                        >
                            <div>
                                <p
                                className='text-white text-xl font-semibold ' 
                                >
                                    {'Nike Air Max 90'}
                                </p>
                            </div>
                            <div
                            className='bg-white h-[2px] w-1/3'
                            >

                            </div>
                        </div>
                        <ArrowRightIcon
                        className='text-white w-6 h-6'
                        />
                    </div>
                </li>
                <li
                className=''
                >
                    <p
                    className='text-orange-300 text-xl font-semibold'
                    >
                        {'Nike Air Max 2005'}
                    </p>
                </li>
            </ul>

        </div>
        <div
        className='w-full h-full relative'
        >
            <div
            className='absolute w-full h-full'
            >
                <Image
                    src={'/Images/jordan.png'}
                    fill
                    className='object-cover opacity-20 z-0'
                    alt='Jordan'
                />
            </div>
            <div
            className='z-40 w-full h-full flex flex-col items-center relative'
            >
                <div
                className='absolute w-2/5 h-2/3 top-3'
                >
                    <Image
                    src={'/Images/jordan1.png'}
                    fill
                    className='object-cover z-50 '
                    alt='Jordan'
                    />
                </div>
                <div
                className='absolute z-40 bg-white w-[40%] h-[200px] bottom-14 flex flex-col justify-end'
                >
                    <div
                                className='bg-black aspect-square w-12 p-1 absolute -right-6 bottom-8'
                                >
                                    <ShoppingCartIcon
                                    className='text-white'
                                    />
                                </div>
                    <div
                    className='flex justify-between items-start pl-8 pb-8 pr-16'
                    >
                        <div
                        className='border border-gray-300 rounded-full px-2 py-1 flex items-center space-x-8'
                        >
                            <p
                            className='text-black'
                            >
                                {'10 US'}
                            </p>
                            <ChevronDownIcon
                            className='text-orange-500 w-5 h-5'
                            />

                        </div>
                        <div
                        className='flex flex-col'
                        >
                            <div
                            className='flex justify-end'
                            >
                                <p
                                className='text-orange-500 text-sm line-through decoration-orange-500'
                                >
                                    {'$179,00'}
                                </p>
                            </div>    
                            <div
                            className='flex items-end'
                            >
                                <p
                                className='text-lg pb-[4px]'
                                >
                                    {'$'}
                                </p>
                                <p
                                className='text-[28px]'
                                >
                                    {'79,00'}
                                </p>
                            </div>
                            
                                
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
  )
}

export default jordan
