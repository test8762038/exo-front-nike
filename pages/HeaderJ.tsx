import { ShoppingCartIcon } from '@heroicons/react/24/outline'
import React from 'react'

function HeaderJ() {
  return (
    <div
    className='w-full h-14 bg-orange-600'
    >
      <nav
        className='h-full w-full flex items-center justify-center space-x-20'
        >
            <div
            className='w-14 h-14 bg-orange-700 scale-125 flex items-center justify-center'
            >
                <div
                className='bg-white rounded-full w-10 h-10 '
                >

                </div>
                
            </div>
            <ul
            className='flex flex-row h-full space-x-10 justify-center items-center'
            >
                <li
                className=''
                >
                    <p
                    className=' text-white font-semibold '
                    >
                        {'MEN'}
                    </p>
                </li>
                <li>
                    <p
                    className=' text-orange-300 font-semibold'
                    >
                        {'WOMEN'}
                    </p>
                </li>
                <li>
                    <p
                    className=' text-orange-300 font-semibold'
                    >
                        {'BOYS'}
                    </p>
                </li>
                <li>
                    <p
                    className=' text-orange-300 font-semibold'
                    >
                        {'GIRLS'}
                    </p>
                </li>
                <li>
                    <p
                    className=' text-orange-300 font-semibold'
                    >
                        {'CUSTOMIZE'}
                    </p>
                </li>
            </ul>
            <div
            className='flex flex-row space-x-3'
            >
                <ShoppingCartIcon
                className='w-7 text-white'
                />

                <div
                className='w-7 h-7 bg-white rounded-full flex justify-center'
                >
                    <p
                    className='text-orange-600 font-semibold'
                    >
                        {'3'}
                    </p>
                </div>
            </div>
        </nav>
    </div>
  )
}

export default HeaderJ
